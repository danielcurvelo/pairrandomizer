//
//  CollectionViewDatasource.h
//  PairRandomizer
//
//  Created by Mac User on 2/21/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;
#import "StudentsViewController.h"
@class Group;

static NSString *cellIdentifier = @"cell";
static NSString *cellIdentifierRight = @"cellRight";
static NSString *cellId = @"cellSquare";
static NSString *const removeEmptyViewNote = @"removeView";
static NSString *const addEmptyViewNote = @"addView";

@interface CollectionViewDatasource : NSObject <UICollectionViewDataSource>

-(void)registerCollectionView:(UICollectionView *)collectionView withGroup:(Group *)group fromTarget:(StudentsViewController *)viewController;

@property (nonatomic,strong) NSIndexPath *arrayIndex;


@end
