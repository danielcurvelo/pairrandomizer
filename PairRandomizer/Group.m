//
//  Group.m
//  PairRandomizer
//
//  Created by Mac User on 4/18/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "Group.h"
#import "Student.h"


@implementation Group

@dynamic name;
@dynamic dateCreated;
@dynamic students;

@end
