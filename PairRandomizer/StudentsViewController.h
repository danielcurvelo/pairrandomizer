//
//  ViewController.h
//  PairRandomizer
//
//  Created by Daniel Curvelo on 2/4/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionViewCell.h"
#import "CollectionViewCellRight.h"
@class Group;
@class Student;

static NSString * const refreshNotification = @"refresh";

@interface StudentsViewController : UIViewController

@property (nonatomic,strong) NSMutableArray * usableArray;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,assign) CGFloat screenHeight;
@property (nonatomic,assign) CGFloat screenWidth;
@property (nonatomic, strong) NSIndexPath * arrayIndex;

@property (nonatomic,strong) Student *studentToDelete;

-(void)refresh:(UIRefreshControl *)refreshControl;
-(void)refreshData;
-(void)updateWithGroup:(Group *)group;

@end

