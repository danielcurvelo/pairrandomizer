//
//  ColorPopPopOverViewController.h
//  PairRandomizer
//
//  Created by Mac User on 6/26/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Student.h"

@interface ColorPopPopOverViewController : UIViewController <UIPopoverPresentationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate>

@property (nonatomic,strong) UICollectionViewCell *selectedCell;
@property (nonatomic,strong) Student *student;

@end

