//
//  NewStudentViewController.h
//  PairRandomizer
//
//  Created by Mac User on 2/7/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudentController.h"

@interface NewStudentViewController : UIViewController
//THIS CLASS WILL BE PART OF THE NEXT VERSION WHERE YOU WILL BE ABLE TO CREATE ALREADY CUSTOMIZED STUDENTS

-(void)SaveData; //Sends the Data to the Model Controller to save the Student
-(void)updateWithGroup:(Group *)group;
@end
