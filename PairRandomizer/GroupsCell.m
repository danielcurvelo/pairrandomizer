//
//  GroupsCell.m
//  PairRandomizer
//
//  Created by Mac User on 4/9/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "GroupsCell.h"
#import "Gradients.h"
#import "UIColor+devMountainColor.h"

@implementation GroupsCell

- (void)awakeFromNib {
    // Initialization code
}

-(instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)drawRect:(CGRect)rect
{
//    CAGradientLayer *bg = [Gradients blueGradient];
//    bg.frame = self.bounds;
//    [self.layer insertSublayer:bg atIndex:0];
//    [self setNeedsLayout];
//    self.iconView = [UIImage ima]
}

-(void)layoutSubviews
{
    self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.colorBar setBackgroundColor:[UIColor blackColor]];
    [self.numberLabel setBackgroundColor:[UIColor blackColor]];
    [self.numberLabel setTextColor:[UIColor darkerColorForColor:[UIColor devMtnBlueColor] forIndex:self.index]];

    [self.numberLabel.layer setBorderColor:[UIColor blackColor].CGColor];
    
    self.layer.shadowOffset = CGSizeMake(0, 1);
    self.layer.shadowColor = [[UIColor blueShadow] CGColor];
    self.layer.shadowRadius = 2;
    self.layer.shadowOpacity = .30f;
    [self.numberLabel setClipsToBounds:YES];
    [self.imageView setTintColor:[UIColor whiteColor]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
