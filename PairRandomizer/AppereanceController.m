//
//  AppereanceController.m
//  PairRandomizer
//
//  Created by Mac User on 2/21/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "AppereanceController.h"
#import "AppDelegate.h"
#import "UIColor+devMountainColor.h"

@implementation AppereanceController

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]]; // DevMountainBlueColor
    [[UINavigationBar appearance] setTintColor:[UIColor devMtnBlueColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"Avenir" size:20]}]; //white tint
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                             forBarMetrics:UIBarMetricsDefault];
    }
    
    return self;
}

@end
