//
//  AlertViewControllers.h
//  PairRandomizer
//
//  Created by Mac User on 4/29/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Group;
@class Student;
@import UIKit;

@interface AlertViewControllers : NSObject

+(instancetype)sharedInstance;

-(UIAlertController *)createGroupAlerView:(UITableView *)tableView;

-(UIAlertController *)EditAlertViewWithGroup:(Group *)group andTableView:(UITableView *)tableView;

-(UIAlertController *)addStudentToGroup:(Group *)group inCollectionView:(UICollectionView *)collectionView;

-(void)removeStudentAlertView:(Student *)student andCollectionView:(UICollectionView *)collectionView atTarget:(UIViewController *)target;

@end
