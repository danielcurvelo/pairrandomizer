//
//  NSDate+FormattedDate.h
//  PairRandomizer
//
//  Created by Mac User on 4/18/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (FormattedDate)

+(NSString *)formattedDate:(NSDate *)date;

@end
