//
//  CollectionViewDatasource.m
//  PairRandomizer
//
//  Created by Mac User on 2/21/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "CollectionViewDatasource.h"
#import "StudentController.h"
#import "AlertViewControllers.h"
#import "StudentsViewController.h"
#import "CollectionViewCell.h"
#import "CollectionViewCellRight.h"
#import "ColorPopPopOverViewController.h"
#import "Student.h"
#import "Group.h"

@interface CollectionViewDatasource () <LeftCellsDelegate,RightCellsDelegate>

@property (nonatomic,strong) CollectionViewCell * cell;
@property (nonatomic,strong) CollectionViewCellRight *cellRight;
@property (nonatomic,strong) Group * group;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) StudentsViewController *target;
@end

@implementation CollectionViewDatasource

@synthesize cell,cellRight;

-(void)registerCollectionView:(UICollectionView *)collectionView withGroup:(Group *)group fromTarget:(StudentsViewController *)viewController
{
    self.target = viewController;
    self.collectionView = collectionView;
    [collectionView registerClass:[CollectionViewCellRight class] forCellWithReuseIdentifier:cellIdentifierRight];
    [collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier];
    self.group = group;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arrayOfStudents = [self.group.students array];
    Student *student = arrayOfStudents[indexPath.row];
    NSString *name = student.name;
    
    
    if (indexPath.row % 2 == 0) {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        [cell setDelegate:self];
        [cell registerCollectionView:collectionView];
        [cell.name setText:name];
        
        if (cell.isRefreshing) {
            [cell setIsRefreshing:NO];
            [UIView animateWithDuration:10/self.group.students.count
                                  delay:indexPath.row/self.group.students.count
                                options:UIViewAnimationOptionAllowUserInteraction
                             animations:^{

                                 [cell setIndex:indexPath.row];
                                 [cell setIndexPath:indexPath];
                                 [cell setGroup:self.group];
                                 [cell.ColorIdentifier setTitle:[NSString stringWithFormat:@"%@",[name substringToIndex:1]] forState:UIControlStateNormal];
                                 [cell updateColors];
                                 [cell setAlpha:1.0];
                                 
                             }
                             completion:^(BOOL finished) {
                                 
                             }];
        }
        
        if (self.group != nil) {

            [cell setIndex:indexPath.row];
            [cell setIndexPath:indexPath];
            [cell setGroup:self.group];
            [cell.ColorIdentifier setTitle:[NSString stringWithFormat:@"%@",[name substringToIndex:1]] forState:UIControlStateNormal];
            [cell updateColors];
        }
        return cell;
    }
    else
    {
        cellRight = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifierRight forIndexPath:indexPath];
        [cellRight setDelegate:self];
        [cellRight registerCollectionView:collectionView];
        [cellRight.name setText:name];

        if (cellRight.isRefreshing) {
            [cellRight setIsRefreshing:NO];
            [UIView animateWithDuration:10/self.group.students.count
                                  delay:indexPath.row/self.group.students.count
                                options:UIViewAnimationOptionAllowUserInteraction
                             animations:^{

                                 [cellRight setIndex:indexPath.row];
                                 [cellRight setIndexPath:indexPath];
                                 [cellRight setGroup:self.group];
                                 [cellRight.ColorIdentifier setTitle:[NSString stringWithFormat:@"%@",[name substringToIndex:1]] forState:UIControlStateNormal];
                                 [cellRight updateColors];
                                 [cell setAlpha:1.0];
                                 
                             }
                             completion:^(BOOL finished) {
                                 
                             }];
        }
        
        if (self.group != nil) {

            [cellRight setIndex:indexPath.row];
            [cellRight setIndexPath:indexPath];
            [cellRight setGroup:self.group];
            [cellRight.ColorIdentifier setTitle:[NSString stringWithFormat:@"%@",[name substringToIndex:1]] forState:UIControlStateNormal];
            [cellRight updateColors];
        }
        return cellRight;
    }
}

-(void)cellHasBeenDeleted:(Student *)student
{
    [[AlertViewControllers sharedInstance] removeStudentAlertView:student andCollectionView:self.collectionView atTarget:self.target];
}

-(void)colorHasBeenPressed:(UIButton *)button andCell:(UICollectionViewCell *)selectedCell
{
    ColorPopPopOverViewController *popover = [[ColorPopPopOverViewController alloc]initWithNibName:@"ColorPopPopOverViewController" bundle:nil];
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:selectedCell];
    popover.student = self.group.students[indexPath.row];
    popover.selectedCell = selectedCell;
    [popover.view setBackgroundColor:[UIColor clearColor]];
    popover.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self.target.navigationController presentViewController:popover animated:YES completion:nil];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (self.group.students.count < 1 || self.group.students == nil) {
        [[NSNotificationCenter defaultCenter] postNotificationName:addEmptyViewNote object:nil];
        return 0;
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:removeEmptyViewNote object:nil];
        return self.group.students.count;
    }
}

@end
