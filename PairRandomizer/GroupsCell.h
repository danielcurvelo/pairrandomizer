//
//  GroupsCell.h
//  PairRandomizer
//
//  Created by Mac User on 4/9/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE

@interface GroupsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *numberLabel;
@property (strong, nonatomic) IBOutlet UIView *colorBar;
@property (strong, nonatomic) IBOutlet UIImageView *iconView;
@property (assign, nonatomic) NSInteger index;

@end
