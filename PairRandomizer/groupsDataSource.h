//
//  groupsDataSource.h
//  PairRandomizer
//
//  Created by Mac User on 4/9/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

static NSString * const emptyGroupViewNotification = @"groupNotification";
static NSString * const addGroupViewNotification = @"groupNotificationAdd";
static NSString * const groupCellId = @"groupCell";

@interface GroupsDataSource : NSObject <UITableViewDataSource>

-(void)registerTableView:(UITableView *)tableView;
@end
