//
//  groupsDataSource.m
//  PairRandomizer
//
//  Created by Mac User on 4/9/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "GroupsDataSource.h"
#import "StudentController.h"
#import "NSDate+FormattedDate.h"
#import "UIColor+devMountainColor.h"
#import "GroupsCell.h"
#import "Group.h"

@implementation GroupsDataSource

-(void)registerTableView:(UITableView *)tableView
{
    [tableView registerNib:[UINib nibWithNibName:@"GroupsCell" bundle:nil] forCellReuseIdentifier:groupCellId];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupsCell *cell = [tableView dequeueReusableCellWithIdentifier:groupCellId forIndexPath:indexPath];
    
    if ([StudentController SharedInstance].groups.count) {
        Group *group = [[StudentController SharedInstance].groups objectAtIndex:indexPath.row];
        if (indexPath.row == 15) {
        }
        [cell setBackgroundColor:[UIColor darkerColorForColor:[UIColor devMtnBlueColor]
                                                     forIndex:indexPath.row]];
//        cell.index = indexPath.row +1;
        cell.nameLabel.text = group.name;
        cell.dateLabel.text = [NSDate formattedDate:group.dateCreated];
        [cell.numberLabel.layer setCornerRadius:cell.numberLabel.frame.size.height/2];
        if (group.students.count > 0 || group.students != nil) {
            cell.numberLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)group.students.count] ;
        }else
        {
        cell.numberLabel.text = @"0";
        }
    }
    
        return cell;
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{

}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([StudentController SharedInstance].groups.count == 0 || [StudentController SharedInstance].groups == nil) {
        [[NSNotificationCenter defaultCenter] postNotificationName:addGroupViewNotification object:nil];
        return 0;
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:emptyGroupViewNotification object:nil];
        return [StudentController SharedInstance].groups.count;
    }
}

@end
