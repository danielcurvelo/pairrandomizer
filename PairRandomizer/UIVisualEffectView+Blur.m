//
//  UIVisualEffectView+Blur.m
//  PairRandomizer
//
//  Created by Mac User on 5/4/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "UIVisualEffectView+Blur.h"

@implementation UIVisualEffectView (Blur)

+(UIVisualEffectView *)darkBlurWithFrame:(CGRect)frame forView:(UIView *)view;
{
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = frame;
    [view setClipsToBounds:YES];
    [view addSubview:effectView];

    return effectView;
}

+(UIVisualEffectView *)lightBlurWithFrame:(CGRect)frame forView:(UIView *)view;
{
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = frame;
    [view setClipsToBounds:YES];
    [view addSubview:effectView];
    
    UIBlurEffect *vibrancyBlurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:vibrancyBlurEffect];
    UIVisualEffectView *vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
    [vibrancyEffectView setFrame:frame];
    [effectView.contentView addSubview:vibrancyEffectView];
    
    return effectView;
}

+(UIVisualEffectView *)extraLightBlurWithFrame:(CGRect)frame forView:(UIView *)view;
{
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = frame;
    [view setClipsToBounds:YES];
    [view addSubview:effectView];
    
    return effectView;
}

@end
