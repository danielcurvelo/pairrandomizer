//
//  NewStudentViewController.m
//  PairRandomizer
//
//  Created by Mac User on 2/7/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "NewStudentViewController.h"
#import "StudentController.h"
#import "StudentsViewController.h"
#import "Group.h"
#import "Student.h"

@interface NewStudentViewController () <UITextFieldDelegate,UIAlertViewDelegate>
//THIS CLASS WILL BE PART OF THE NEXT VERSION WHERE YOU WILL BE ABLE TO CREATE ALREADY CUSTOMIZED STUDENTS
@property (nonatomic,strong) UITextField * name;
@property (nonatomic,strong) StudentsViewController *vc;
@property (nonatomic,strong) Group *group;
@end

@implementation NewStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vc = [StudentsViewController new];
    
    [self setupNavigationBar];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.name = [[UITextField alloc]initWithFrame:CGRectMake(20, 90, self.view.frame.size.width - 40, 30)];
    [self.name setBorderStyle:UITextBorderStyleRoundedRect];
    
    [self.name setPlaceholder:@"Name"];
    [self.view addSubview:self.name];
    
    UIButton *doneBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.name.center.x - 50, 150 , 100, 40)];
    [doneBtn addTarget:self action:@selector(SaveData) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setBackgroundColor:[UIColor colorWithRed:0.16 green:0.67 blue:0.89 alpha:1]];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn.layer setCornerRadius:10];
    
    [self.view addSubview:doneBtn];
    
    // Do any additional setup after loading the view.
}

-(void)updateWithGroup:(Group *)group
{
    self.group = group;
}

-(void)SaveData
{
    if ([self.name text].length > 0) {
    
    [[StudentController SharedInstance] addStudent:[self.name text] toGroup:self.group];
    
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Needs a Name" message:@"Please add a name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)setupNavigationBar
{
    [self setTitle:@"Add Student"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
