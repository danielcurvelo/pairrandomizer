//
//  Gradients.m
//  PairRandomizer
//
//  Created by Mac User on 4/24/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "Gradients.h"
#import "UIColor+devMountainColor.h"
@import UIKit;

@implementation Gradients

+(CAGradientLayer *)blueGradient
{
UIColor * colorOne = [UIColor devMtnBlueColor];
UIColor * colorTwo = [UIColor devMtnDarkBlueColor];

NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];

NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];

NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];

CAGradientLayer *headerLayer = [CAGradientLayer layer];
headerLayer.colors = colors;
headerLayer.locations = locations;

return headerLayer;
}
@end
