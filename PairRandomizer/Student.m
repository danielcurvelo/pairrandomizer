//
//  Student.m
//  PairRandomizer
//
//  Created by Mac User on 4/28/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "Student.h"
#import "Group.h"


@implementation Student

@dynamic name;
@dynamic color;
@dynamic group;

@end
