//
//  StudentController.m
//  PairRandomizer
//
//  Created by Mac User on 2/16/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "StudentController.h"
#import "Stack.h"
#import "Student.h"
#import "Group.h"

@implementation StudentController

+(StudentController *)SharedInstance
{
    static StudentController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
    sharedInstance = [[StudentController alloc] init];
    });
    
    return sharedInstance;
}

-(void)incrementAppLaunches:(NSInteger)launch
{
    // MARK: Keeping track of App Launches.
    launch++;
    [[NSUserDefaults standardUserDefaults] setInteger:launch forKey:@"launches"];
}

-(NSInteger)launches
{
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"launches"]) {
        return 0;
    }
    else
    {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"launches"];
    }
}

-(NSArray *)groups
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Group"];
    NSArray *objects = [[Stack sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:NULL];
    return objects;
}

-(void)addGroupWithName:(NSString *)name
{
    Group *group = [NSEntityDescription
                        insertNewObjectForEntityForName:@"Group"
                        inManagedObjectContext:[Stack sharedInstance].managedObjectContext];
    group.name = name;
    NSDate *date = [NSDate date];
    group.dateCreated = date;
    
    [self save];
}

-(void)editGroup:(Group *)group withName:(NSString *)name
{
    group.name = name;
    [self save];
}


-(void)addStudent:(NSString *)name toGroup:(Group *)group
{
    
    Student *student = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Student"
                    inManagedObjectContext:[Stack sharedInstance].managedObjectContext];
    student.name = name;
    student.group = group;
    
    [self save];
}

-(void)removeStudent:(Student *)student
{
    [[Stack sharedInstance].managedObjectContext deleteObject:student];
    [self save];
}

-(void)changeColorToStudent:(NSInteger)color toStudent:(Student *)student
{
    student.color = [NSNumber numberWithInteger:color];
    [self save];
}

-(void)removeGroup:(Group *)group
{
    [[Stack sharedInstance].managedObjectContext deleteObject:group];
    
    [self save];
    
}


- (void)shuffle:(NSArray *)array fromGroup:(Group *)group
{
    NSMutableOrderedSet *newArray = [[NSMutableOrderedSet alloc]initWithArray:array];
    NSUInteger count = [newArray count];
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [newArray exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
    group.students = newArray;
    [self save];
}

- (void)save {
    
    [[Stack sharedInstance].managedObjectContext save:NULL];
    
}


@end
