//
//  UIColor+devMountainColor.m
//  PairRandomizer
//
//  Created by Mac User on 2/21/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "UIColor+devMountainColor.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIColor (devMountainColor)

+(UIColor *)devMtnBlueColor
{
    return [UIColor colorWithRed:0.16 green:0.67 blue:0.89 alpha:1];
}

+ (UIColor *)darkerColorForColor:(UIColor *)color forIndex:(NSInteger)index
{
    NSString *floatNumber = [NSString stringWithFormat:@"0.%ld",(long)index];
   float dynamicNumber = [floatNumber floatValue];
    CGFloat r, g, b, a;
    
    if ([color getRed:&r green:&g blue:&b alpha:&a]){
        return [UIColor colorWithRed:MAX(r - dynamicNumber/2, 0.0)
                               green:MAX(g - dynamicNumber/2, 0.0)
                                blue:MAX(b - dynamicNumber/2, 0.0)
                               alpha:a];
    }
    return nil;
}

+(UIColor *)lighterColorForColor:(UIColor *)color forIndex:(NSInteger)index
{
    NSString *floatNumber = [NSString stringWithFormat:@"0.%ld",(long)index];
    float dynamicNumber = [floatNumber floatValue];
    CGFloat r, g, b, a;
    
    if ([color getRed:&r green:&g blue:&b alpha:&a]){
        return [UIColor colorWithRed:MAX(r + dynamicNumber/2, 0.0)
                               green:MAX(g + dynamicNumber/2, 0.0)
                                blue:MAX(b + dynamicNumber/2, 0.0)
                               alpha:a];
    }
    return nil;
}

+(UIColor *)devMtnDarkBlueColor
{
    return [UIColor colorWithRed:0.16 green:0.68 blue:0.92 alpha:1];
}

+(UIColor *)devMtnGrayColor
{
    return [UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1];
}

+(UIColor *)yellowOrange
{
    return [UIColor colorWithRed:1 green:0.68 blue:0.26 alpha:1];
}

+(UIColor *)blueShadow
{
    return [UIColor colorWithRed:0.16 green:0.21 blue:0.32 alpha:1];
}

+(UIColor *)yellowShadow
{
    return [UIColor colorWithRed:0.76 green:0.51 blue:0.2 alpha:1];
}

+(UIColor *)darkerBlue
{
    return [UIColor colorWithRed:0.35 green:0.45 blue:0.6 alpha:1];
}

+(UIColor *)darkerGreen
{
    return [UIColor colorWithRed:0.04 green:0.44 blue:0.33 alpha:1];
}

+(UIColor *)grassGreen
{
    return [UIColor colorWithRed:0.4 green:0.6 blue:0.4 alpha:1];
}

+(UIColor *)mahoganyRed
{
    return [UIColor colorWithRed:0.76 green:0.19 blue:0 alpha:1];
}

+(UIColor *)totemPoleRed
{
    return [UIColor colorWithRed:0.61 green:0.16 blue:0 alpha:1];
}

+(UIColor *)fadedBlue
{
    return [UIColor colorWithRed:0.55 green:0.53 blue:1 alpha:1];
}

+(UIColor *)stoneBlue
{
    return [UIColor colorWithRed:0 green:0.4 blue:0.4 alpha:1];
}

+(UIColor *)arapawa
{
    return [UIColor colorWithRed:0 green:0 blue:0.4 alpha:1];
}

+(UIColor *)grannySmith
{
    return [UIColor colorWithRed:0.46 green:0.6 blue:0.56 alpha:1];
}

+(UIColor *)allportsBlue
{
    return [UIColor colorWithRed:0 green:0.47 blue:0.6 alpha:1];
}

+(UIColor *)waxFlower
{
    return [UIColor colorWithRed:0.95 green:0.73 blue:0.66 alpha:1];
}

+(UIColor *)selectionColorAtIndex:(NSInteger)index
{
    static NSArray *choices = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        choices = [self arrayOfColors];
    });

    return choices[index];
    
}

+(NSArray *)arrayOfColors
{
    return @[
             [UIColor devMtnBlueColor],
             [UIColor yellowShadow],
             [UIColor blueShadow],
             [UIColor devMtnGrayColor],
             [UIColor darkerBlue],
             [UIColor purpleColor],
             [UIColor lightGrayColor],
             [UIColor blackColor],
             [UIColor grassGreen],
             [UIColor mahoganyRed],
             [UIColor totemPoleRed],
             [UIColor fadedBlue],
             [UIColor stoneBlue],
             [UIColor arapawa],
             [UIColor grannySmith],
             [UIColor waxFlower]
             ];
}

@end
