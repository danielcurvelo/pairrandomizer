//
//  StudentController.h
//  PairRandomizer
//
//  Created by Mac User on 2/16/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Student;
@class Group;

static NSString * const studentKey = @"studentKey";

@interface StudentController : NSObject

@property (nonatomic,strong) NSArray * students;
@property (nonatomic,strong) NSArray * groups;
@property (nonatomic,strong) Group *currentGroup;
@property (nonatomic, assign) NSInteger launches;
+(StudentController *)SharedInstance;

-(void)incrementAppLaunches:(NSInteger)launch;

-(void)removeStudent:(Student *)student;

-(void)removeGroup:(Group *)group;

-(void)editGroup:(Group *)group withName:(NSString *)name;

-(void)addStudent:(NSString *)name toGroup:(Group *)group;

//-(void)shuffle:(NSArray *)array;
- (void)shuffle:(NSArray *)array fromGroup:(Group *)group;
-(void)addGroupWithName:(NSString *)name;
-(void)changeColorToStudent:(NSInteger)color toStudent:(Student *)student;

@end
