//
//  ColorPopPopOverViewController.m
//  PairRandomizer
//
//  Created by Mac User on 6/26/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "ColorPopPopOverViewController.h"
#import "UIColor+devMountainColor.h"
#import "CollectionViewCell.h"
#import "CollectionViewCellRight.h"
#import "UIVisualEffectView+Blur.h"
#import "StudentController.h"
@interface ColorPopPopOverViewController ()
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ColorPopPopOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.collectionView setBackgroundView:[UIVisualEffectView darkBlurWithFrame:self.collectionView.bounds forView:self.collectionView.backgroundView]];
    [self.collectionView.layer setCornerRadius:15];
    [self.collectionView.layer setBorderWidth:1];
    [self.collectionView.layer setBorderColor:[UIColor whiteColor].CGColor];

}
- (IBAction)viewTapped:(id)sender {
    [self dismissModal];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint touchLocation = [touch locationInView:self.view];
    return !CGRectContainsPoint(self.collectionView.frame, touchLocation);
}

-(void)dismissModal
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [UIColor arrayOfColors].count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"colorCell"];
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"colorCell" forIndexPath:indexPath];
    [cell.layer setCornerRadius:cell.frame.size.width/2];
    [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
    [cell.layer setBorderWidth:1];
    [cell setBackgroundColor:[UIColor selectionColorAtIndex:indexPath.row]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        CollectionViewCell * leftCell = (CollectionViewCell *)self.selectedCell;
        [leftCell.ColorIdentifier setBackgroundColor:[UIColor selectionColorAtIndex:indexPath.row]];
        [[StudentController SharedInstance] changeColorToStudent:indexPath.row toStudent:self.student];
    }
    else
    {
        CollectionViewCellRight * rightCell = (CollectionViewCellRight *)self.selectedCell;
        [rightCell.ColorIdentifier setBackgroundColor:[UIColor arrayOfColors][indexPath.item]];
        [[StudentController SharedInstance] changeColorToStudent:indexPath.row toStudent:self.student];
    }
    
    [self dismissModal];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(25 , 10, 25, 10);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2; // This is the minimum inter item spacing, can be more
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width / 4)- 15, (collectionView.frame.size.width / 4)-15);
}
@end
