//
//  Gradients.h
//  PairRandomizer
//
//  Created by Mac User on 4/24/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface Gradients : NSObject

+(CAGradientLayer *)blueGradient;

@end
