//
//  UIColor+devMountainColor.h
//  PairRandomizer
//
//  Created by Mac User on 2/21/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (devMountainColor)

+(NSArray *)arrayOfColors;

+(UIColor *)devMtnBlueColor;

+(UIColor *)darkerColorForColor:(UIColor *)color forIndex:(NSInteger)index;

+(UIColor *)lighterColorForColor:(UIColor *)color forIndex:(NSInteger)index;

+(UIColor *)devMtnGrayColor;

+(UIColor *)devMtnDarkBlueColor;

+(UIColor *)yellowOrange;

+(UIColor *)darkerBlue;

+(UIColor *)blueShadow;

+(UIColor *)yellowShadow;

+(UIColor *)grassGreen;

+(UIColor *)mahoganyRed;

+(UIColor *)totemPoleRed;

+(UIColor *)fadedBlue;

+(UIColor *)stoneBlue;

+(UIColor *)arapawa;

+(UIColor *)grannySmith;

+(UIColor *)allportsBlue;

+(UIColor *)waxFlower;

+(UIColor *)selectionColorAtIndex:(NSInteger)index;

@end
