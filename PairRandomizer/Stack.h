//
//  Stack.h
//  PairRandomizer
//
//  Created by Mac User on 4/9/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//
#import <Foundation/Foundation.h>
@import CoreData;

@interface Stack : NSObject

+ (Stack *)sharedInstance;

@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;

@end