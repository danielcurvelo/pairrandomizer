//
//  CollectionViewCellRight.h
//  PairRandomizer
//
//  Created by Mac User on 4/27/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Group;
@class Student;
@protocol RightCellsDelegate;

@interface CollectionViewCellRight : UICollectionViewCell

@property (nonatomic,weak) id<RightCellsDelegate> delegate;
@property (nonatomic,strong) UILabel *name;
@property (nonatomic,assign) CGRect labelFrame;
@property (nonatomic,assign) BOOL isOdd;
@property (nonatomic,strong) UIButton *ColorIdentifier;
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,assign) Group * group;
@property (nonatomic,assign) BOOL isRefreshing;
@property (nonatomic,assign) NSIndexPath *indexPath;

-(void)setupViews;
//-(void)updateLabelFrame:(CGRect)frame;

-(void)shakeAnimation:(void (^)(BOOL completion))completion;
-(void)startJiggling;
-(void)updateColors;
-(void)registerCollectionView:(UICollectionView *)collectionView;

@end

@protocol RightCellsDelegate <NSObject>

-(void)cellHasBeenDeleted:(Student *)student;
-(void)colorHasBeenPressed:(UIButton *)button andCell:(UICollectionViewCell *)selectedCell;

@end