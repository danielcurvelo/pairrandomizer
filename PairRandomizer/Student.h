//
//  Student.h
//  PairRandomizer
//
//  Created by Mac User on 4/28/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Group;

@interface Student : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * color;
@property (nonatomic, retain) Group *group;

@end
