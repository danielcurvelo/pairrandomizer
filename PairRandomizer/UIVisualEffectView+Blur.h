//
//  UIVisualEffectView+Blur.h
//  PairRandomizer
//
//  Created by Mac User on 5/4/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIVisualEffectView (Blur)

+(UIVisualEffectView *)darkBlurWithFrame:(CGRect)frame forView:(UIView *)view;

+(UIVisualEffectView *)lightBlurWithFrame:(CGRect)frame forView:(UIView *)view;

+(UIVisualEffectView *)extraLightBlurWithFrame:(CGRect)frame forView:(UIView *)view;

@end
