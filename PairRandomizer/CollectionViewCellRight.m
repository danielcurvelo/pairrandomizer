//
//  CollectionViewCellRight.m
//  PairRandomizer
//
//  Created by Mac User on 4/27/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "CollectionViewCellRight.h"
#import "CollectionViewCell.h"
#import "UIColor+devMountainColor.h"
#import "StudentsViewController.h"
#import "StudentController.h"
#import "UIVisualEffectView+Blur.h"
#import "Student.h"
#import "Group.h"
@import AudioToolbox;

@interface CollectionViewCellRight ()
@property (nonatomic,assign) CGRect originalFrame;
@property (nonatomic,assign) NSInteger selectionIndex;
@property (nonatomic,strong) UICollectionView *collectionView;
@end

@implementation CollectionViewCellRight

@synthesize name;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UILongPressGestureRecognizer *deleteGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(holdToDelete:)];
        deleteGesture.minimumPressDuration = 0.5;
        deleteGesture.allowableMovement = 600;
        [self addGestureRecognizer:deleteGesture];
        
        [self setupViews];
        self.ColorIdentifier = [[UIButton alloc]initWithFrame:CGRectMake(5, self.frame.size.height / 2 - 20, 40, 40)];
        [self.ColorIdentifier.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.ColorIdentifier.titleLabel setTextColor:[UIColor whiteColor]];
        [self.ColorIdentifier.layer setBorderColor:[UIColor whiteColor].CGColor];
        [self.ColorIdentifier.layer setBorderWidth:1];
        
        [self.ColorIdentifier.layer setCornerRadius:20];
        self.ColorIdentifier.clipsToBounds = YES;
        
        [self.ColorIdentifier addTarget:self action:@selector(colorButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.ColorIdentifier];
        
        self.originalFrame = frame;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shakeAnimation:) name:shakeAnimation object:nil];
        
    }
    return self;
}




-(void)layoutSubviews{


}

-(void)registerCollectionView:(UICollectionView *)collectionView
{
    self.collectionView = collectionView;
}

-(void)holdToDelete:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [self performSelector:@selector(vibrateAndDelete) withObject:nil afterDelay:0.5];
    }
}

-(void)vibrateAndDelete
{
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    Student *student = [self.group.students objectAtIndex:self.index];
    [self.delegate cellHasBeenDeleted:student];
}

-(void)setupViews
{
//    self.backgroundColor = [UIColor devMtnBlueColor];
    [self.layer setBorderColor:[UIColor devMtnBlueColor].CGColor];
    [self.layer setBorderWidth:1.0];
    [self.layer setCornerRadius:6.0];
    
//    [UIVisualEffectView lightBlurWithFrame:self.bounds forView:self];
    
    self.labelFrame = CGRectMake(45 , 0, self.bounds.size.width - 55, self.bounds.size.height);
    name = [[UILabel alloc]initWithFrame:self.labelFrame];
//    [name setOpaque:YES];
//    [name setShadowOffset:CGSizeMake(0, 1)];
//    [name setShadowColor:[UIColor blueShadow]];
    name.layer.masksToBounds = NO;
    [name setTextAlignment:NSTextAlignmentRight];
    [name setTextColor:[UIColor whiteColor]];
    [name setFont:[UIFont fontWithName:@"Avenir" size:20]];
    [self addSubview:name];
    
    [self setupParallaxEffect];
}

-(void)setupParallaxEffect
{
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-10);
    verticalMotionEffect.maximumRelativeValue = @(10);
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-10);
    horizontalMotionEffect.maximumRelativeValue = @(10);
    
    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    
    // Add both effects to your view
    [self addMotionEffect:group];
}

-(void)updateColors
{
    Student *student = [self.group.students objectAtIndex:self.index];
    self.selectionIndex = [student.color integerValue];
    [self.ColorIdentifier setBackgroundColor:[UIColor selectionColorAtIndex:self.selectionIndex]];
}

-(void)updateLabelFrame:(CGRect)frame
{
    [name setFrame:frame];
    [name setNeedsLayout];
}

-(void)shakeAnimation:(void (^)(BOOL completion))completion
{
    CAKeyframeAnimation *keyFrameAnimation = [CAKeyframeAnimation animation];
    keyFrameAnimation.keyPath = @"position.x";
    keyFrameAnimation.values = @[ @0, @10, @-20, @50, @0 ];
    keyFrameAnimation.keyTimes = @[ @0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1 ];
    keyFrameAnimation.duration = 1.0;
    
    keyFrameAnimation.additive = YES;
    
    [self.layer addAnimation:keyFrameAnimation forKey:@"shake"];
    completion(YES);
    //        [self.smallSquare.layer addAnimation:keyFrameAnimation forKey:@"shake"]
}

-(void)colorButtonSelected:(UIButton *)button
{
    [self.delegate colorHasBeenPressed:button andCell:self];
}

-(void)toggleColors
{
    if (self.selectionIndex <= 13) {
        self.selectionIndex++;
        [self.ColorIdentifier setBackgroundColor:[UIColor selectionColorAtIndex:self.selectionIndex]];
        Student *student = [self.group.students objectAtIndex:self.index];
        [[StudentController SharedInstance] changeColorToStudent:self.selectionIndex toStudent:student];
    }
    else
    {
        self.selectionIndex = 0;
        [self.ColorIdentifier setBackgroundColor:[UIColor selectionColorAtIndex:self.selectionIndex]];
        Student *student = [self.group.students objectAtIndex:self.index];
        [[StudentController SharedInstance] changeColorToStudent:self.selectionIndex toStudent:student];
    }
}


-(void)startJiggling
{
    float delay = 0;
    
    [self setIsRefreshing:YES];
    [UIView animateWithDuration:0.3
                          delay:delay
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         
                         [self setAlpha:0.0];
                         
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
//                             [self.collectionView reloadItemsAtIndexPaths:@[self.indexPath]];
                             [UIView animateWithDuration:0.3 animations:^{
                                 
                                 [self refreshCollectionView];
                                 [self setAlpha:1.0];
                             }];
                             
                         }
                         
    }];
    
}

-(void)refreshCollectionView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:refreshNotification object:nil];


}

@end
