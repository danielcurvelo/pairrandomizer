//
//  ViewController.m
//  PairRandomizer
//
//  Created by Daniel Curvelo on 2/4/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "StudentsViewController.h"
#import "CollectionViewCell.h"
#import "AlertViewControllers.h"
#import "NewStudentViewController.h"
#import "UIColor+devMountainColor.h"
#import "UIVisualEffectView+Blur.h"
#import "Gradients.h"
#import "CollectionViewDatasource.h"
#import "ColorPopPopOverViewController.h"
#import "StudentController.h"
#import "GroupsCell.h"
#import "Student.h"
#import "Group.h"

@interface StudentsViewController () <UICollectionViewDelegateFlowLayout, UIAlertViewDelegate, UIPopoverPresentationControllerDelegate>

@property (nonatomic,strong) CollectionViewDatasource * dataSource;
@property (nonatomic,strong) StudentController *modelController;
@property (nonatomic,strong) CollectionViewCell * customCell;
@property (nonatomic,strong) Group *group;
@property (nonatomic,strong) NewStudentViewController *addStudentController;
@property (nonatomic,strong) UIButton *emptyView;
@property (nonatomic,strong) id notification;

@end

@implementation StudentsViewController

@synthesize screenHeight,screenWidth,customCell;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNotifications];

    [self.view setBackgroundColor:[UIColor blackColor]];
    self.usableArray = [NSMutableArray arrayWithArray:[self.group.students array]];
    self.dataSource = [CollectionViewDatasource new];
    
    screenHeight = self.view.frame.size.height;
    screenWidth = self.view.frame.size.width;

    
    [self setupNavigationBar];

    [self setupCollectionView];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self refreshData];
    [self setupRefreshBtn];
    
}


-(void)setupEmptyView
{
    if (self.group.students.count == 0 || !self.group.students.count) {
        self.emptyView = [[UIButton alloc]initWithFrame:self.view.bounds];
        [self.emptyView addTarget:self action:@selector(AddNewStudent) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake(self.emptyView.center.x - 100 , self.emptyView.center.y - 50, 200, 100)];
        [message setTextAlignment:NSTextAlignmentCenter];
        [message setNumberOfLines:0];
        [message setTextColor:[UIColor whiteColor]];
        [message setText:@"You have nobody in this group, add someone."];
        
        UIImage * image = [UIImage imageNamed:@"addGroup2"];
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        UIImageView *imageAdd = [[UIImageView alloc]initWithFrame:CGRectMake(self.emptyView.center.x - 17.5, message.frame.origin.y - 17.5, 35, 35)];
        [imageAdd setContentMode:UIViewContentModeCenter];
        [imageAdd setTintColor:[UIColor whiteColor]];
        [imageAdd setImage:image];
        
        [self.view addSubview:self.emptyView];
        [self.emptyView addSubview:message];
        [self.emptyView addSubview:imageAdd];
        
    }
    else
    {
        if (self.emptyView) {
            [self.emptyView removeFromSuperview];
        }
    }
}

#pragma mark - Setup CollectionView

-(void)setupCollectionView
{
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.bounds.size.width, self.view.bounds.size.height) collectionViewLayout:layout];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.dataSource registerCollectionView:self.collectionView withGroup:self.group fromTarget:self];
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
}

-(void)updateWithGroup:(Group *)group
{
    self.group = group;
    [self setTitle:group.name];
    [self.collectionView reloadData];
}

#pragma mark - UICollectionDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    

        CGSize itemSize = CGSizeMake((screenWidth /2) - 6 , ((screenHeight - 64) / 5) / 2);
        return itemSize;

    
}

-(void)setupRefreshBtn
{
    UIButton * refreshButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.center.x - 25, self.view.frame.size.height - 55,  50, 50)];
    
    UIImage *image = [UIImage imageNamed:@"refreshIcon"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [refreshButton.titleLabel setContentMode:UIViewContentModeCenter];
    [refreshButton setImage:image forState:UIControlStateNormal];
    [refreshButton setTintColor:[UIColor blackColor]];
    [refreshButton setBackgroundColor:[UIColor devMtnBlueColor]];
    [refreshButton.layer setCornerRadius:refreshButton.frame.size.width/2];
    [refreshButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [refreshButton.layer setShadowOffset:CGSizeMake(1, 3)];
    [refreshButton.layer setShadowOpacity:0.6];
    [refreshButton.layer setShadowColor:[UIColor blackColor].CGColor];
    [refreshButton.layer setShadowRadius:2];
    
    [refreshButton addTarget:self action:@selector(animateCells) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:refreshButton];
    [self.view bringSubviewToFront:refreshButton];

}


- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationPopover;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(4, 4, 40, 4);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout maximumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}


#pragma mark - Setup Navigation Bar

-(void)setupNavigationBar
{
    
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc]
                               initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                               target:self
                               action:@selector(AddNewStudent)];
    
    self.navigationItem.rightBarButtonItem = addBtn;
    
}

#pragma mark - Shuffle & Refresh Methods

-(void)refresh:(UIRefreshControl *)refreshControl
{
    [[StudentController SharedInstance] shuffle:[self.group.students array] fromGroup:self.group];
    [self refreshData];
    
    if (refreshControl != nil) {
        [refreshControl endRefreshing];
    }

}

-(void)refresh
{
    [[StudentController SharedInstance] shuffle:[self.group.students array] fromGroup:self.group];
    [self refreshData];
}

-(void)animateCells
{
    [[StudentController SharedInstance] shuffle:[self.group.students array] fromGroup:self.group];
//    NSIndexPath * indexPath = [NSIndexPath indexPathWithIndex:0];
//    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    [self.collectionView.visibleCells  makeObjectsPerformSelector:@selector(startJiggling)];
}

-(void)refreshData
{
    [self.collectionView reloadData];
}

-(void)AddNewStudent
{
   UIAlertController * alert = [[AlertViewControllers sharedInstance] addStudentToGroup:self.group inCollectionView:self.collectionView];
    [self.emptyView removeFromSuperview];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Notification Center

-(void)registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupEmptyView) name:addEmptyViewNote object:nil];
    self.notification = [[NSNotificationCenter defaultCenter] addObserverForName:removeEmptyViewNote object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        if ([note.name isEqualToString:removeEmptyViewNote]) {
            [self.emptyView removeFromSuperview];
        }
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:refreshNotification object:nil];
}

-(void)unregisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:studentKey object:nil];
    [self.notification removeObserver:self name:removeEmptyViewNote object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:addEmptyViewNote object:nil];
}

-(void)dealloc
{
    [self unregisterNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
