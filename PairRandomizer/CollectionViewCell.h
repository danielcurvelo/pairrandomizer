//
//  CollectionViewCell.h
//  PairRandomizer
//
//  Created by Mac User on 2/6/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Group;
@class Student;

static NSString * const shakeAnimation = @"shake";

@protocol LeftCellsDelegate;

@interface CollectionViewCell : UICollectionViewCell

@property (nonatomic,weak) id<LeftCellsDelegate> delegate;
@property (nonatomic,strong) UILabel *name;
@property (nonatomic,assign) CGRect labelFrame;
@property (nonatomic,assign) BOOL isOdd;
@property (nonatomic,strong) UIButton *ColorIdentifier;
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,assign) Group * group;
@property (nonatomic,assign) BOOL isRefreshing;
@property (nonatomic,assign) NSIndexPath *indexPath;

-(void)setupViews;
//-(void)updateLabelFrame:(CGRect)frame;

-(void)shakeAnimation:(void (^)(BOOL completion))completion;
-(void)startJiggling;
-(void)updateColors;
-(void)registerCollectionView:(UICollectionView *)collectionView;

@end

@protocol LeftCellsDelegate <NSObject>

-(void)cellHasBeenDeleted:(Student *)student;
-(void)colorHasBeenPressed:(UIButton *)button andCell:(UICollectionViewCell *)selectedCell;
@end
