//
//  GroupViewController.m
//  PairRandomizer
//
//  Created by Mac User on 4/20/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "GroupViewController.h"
#import "GroupsDataSource.h"
#import "StudentController.h"
#import "StudentsViewController.h"
#import "Group.h"
#import "UIColor+devMountainColor.h"
#import "AlertViewControllers.h"
#import "ColorPopPopOverViewController.h"
#import "OnboardingViewController.h"

@interface GroupViewController ()

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) GroupsDataSource *dataSource;
@property (nonatomic,strong) NSString *groupName;
@property (nonatomic,strong) UIAlertController *controller;
@property (nonatomic,strong) UIButton *emptyView;
@property (nonatomic,strong) OnboardingViewController *onboarding;
@property (nonatomic,strong) id notification;
@end

@implementation GroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self registerNotifications];
    self.groupName = @"New Group";
    self.dataSource = [GroupsDataSource new];
    self.tableView = [[UITableView alloc]initWithFrame:self.view.bounds];
    self.tableView.contentMode = UIViewAutoresizingFlexibleWidth;
    [self.tableView setBackgroundColor:[UIColor blackColor]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self.dataSource;
    [self.dataSource registerTableView:self.tableView];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];
    [self setupNavigationBar];
    
    [self setupEmptyView];
    self.onboarding = [OnboardingViewController new];
    self.onboarding.shown = NO;

    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    NSInteger launches = [StudentController SharedInstance].launches;
    if (launches < 2 && self.onboarding.shown == NO) {
        [self.onboarding setShown:YES];
        [self.navigationController presentViewController:self.onboarding animated:YES completion:nil];
    }
    
}

-(void)setupEmptyView
{
    if ([StudentController SharedInstance].groups.count == 0 || ![StudentController SharedInstance].groups.count) {
        self.emptyView = [[UIButton alloc]initWithFrame:self.view.bounds];
        [self.emptyView addTarget:self action:@selector(NewGroup) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake(self.emptyView.center.x - 100 , self.emptyView.center.y - 50, 200, 100)];
        [message setTextAlignment:NSTextAlignmentCenter];
        [message setNumberOfLines:0];
        [message setTextColor:[UIColor whiteColor]];
        [message setText:@"Welcome. You can start by adding a group!"];
        
        UIImage * image = [UIImage imageNamed:@"addGroup2"];
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        UIImageView *imageAdd = [[UIImageView alloc]initWithFrame:CGRectMake(self.emptyView.center.x - 17.5, message.frame.origin.y - 17.5, 35, 35)];
        [imageAdd setContentMode:UIViewContentModeCenter];
        [imageAdd setTintColor:[UIColor whiteColor]];
        [imageAdd setImage:image];
        
        [self.view addSubview:self.emptyView];
        [self.emptyView addSubview:message];
        [self.emptyView addSubview:imageAdd];
        
    }
    else
    {
        if (self.emptyView) {
            [self.emptyView removeFromSuperview];
        }
    }
}

-(void)setupNavigationBar
{
    
    UIImage * image = [UIImage imageNamed:@"addGroup2"];
    
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc]
                               initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(NewGroup)];
    
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"navBarIcon"]];
    UIBarButtonItem *logoLeft = [[UIBarButtonItem alloc]initWithCustomView:imageView];

    UIBarButtonItem *help = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"helpIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(ShowOnBoard)];
    
    self.navigationItem.leftBarButtonItem = logoLeft;
    self.navigationItem.rightBarButtonItems = @[addBtn,help];

}

-(void)ShowOnBoard
{
    [self.navigationController presentViewController:self.onboarding animated:YES completion:nil];
}

-(void)NewGroup
{
    
    UIAlertController *controller = [[AlertViewControllers sharedInstance] createGroupAlerView:self.tableView];
    [self presentViewController:controller animated:YES completion:nil];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    StudentsViewController *studentController = [StudentsViewController new];
    [studentController updateWithGroup:[[StudentController SharedInstance].groups objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:studentController animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Group *group = [[StudentController SharedInstance].groups objectAtIndex:indexPath.row];
    
    UITableViewRowAction *delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                        [[StudentController SharedInstance] removeGroup:group];
                                        [tableView reloadData];
                                    }];
    
    UITableViewRowAction *edit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                  {
                                      Group *group = [[StudentController SharedInstance].groups objectAtIndex:indexPath.row];
                                      UIAlertController *editController = [[AlertViewControllers sharedInstance] EditAlertViewWithGroup:group andTableView:self.tableView];
                                      
                                      [self presentViewController:editController animated:YES completion:nil];
                                  }];
    
    return @[delete,edit];
    
}

-(void)reappearEmtpyView
{
    [self.emptyView setAlpha:1];
}

-(void)registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reappearEmtpyView) name:addGroupViewNotification object:nil];
    self.notification = [[NSNotificationCenter defaultCenter] addObserverForName:emptyGroupViewNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self.emptyView setAlpha:0];
    }];
}

-(void)dealloc
{
    [self.notification removeObserver:self name:emptyGroupViewNotification object:nil];
    [self.notification removeObserver:self name:addGroupViewNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

