//
//  AlertViewControllers.m
//  PairRandomizer
//
//  Created by Mac User on 4/29/15.
//  Copyright (c) 2015 DevMountain. All rights reserved.
//

#import "AlertViewControllers.h"
#import "StudentController.h"
#import "UIColor+devMountainColor.h"
#import "GroupsDataSource.h"
#import "Group.h"
#import "Student.h"

@import UIKit;

@implementation AlertViewControllers

+(instancetype)sharedInstance
{
static AlertViewControllers *sharedInstance = nil;
static dispatch_once_t onceToken;
dispatch_once(&onceToken, ^{
    sharedInstance = [AlertViewControllers new];
});
    return sharedInstance;

}

-(UIAlertController *)createGroupAlerView:(UITableView *)tableView
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle: @"New Group!"
                                                          message: @"Add a name to your new Group."
                                                   preferredStyle: UIAlertControllerStyleAlert];
    
    [controller addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        textField.placeholder = @"Name";
        textField.textColor = [UIColor allportsBlue];
    }];
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle: @"Save"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction *action) {
                                                            [[StudentController SharedInstance] addGroupWithName:[[controller.textFields firstObject] text]];
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:emptyGroupViewNotification object:nil];
                                                            [tableView reloadData];
                                                        }];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: @"Cancel"
                                                           style: UIAlertActionStyleCancel
                                                         handler: ^(UIAlertAction *action) {
                                                         }];
    [controller addAction: cancelAction];
    [controller addAction: alertAction];
    return controller;
}

-(UIAlertController *)EditAlertViewWithGroup:(Group *)group andTableView:(UITableView *)tableView
{
    UIAlertController *editController = [UIAlertController alertControllerWithTitle: @"Edit"
                                                                            message: @"Replace the name of this Group."
                                                                     preferredStyle: UIAlertControllerStyleAlert];
    
    [editController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        textField.text = group.name;
        textField.placeholder = @"Name";
        textField.textColor = [UIColor allportsBlue];
    }];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle: @"Save"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction *action) {
                                                            [[StudentController SharedInstance] editGroup:group withName:[[editController.textFields firstObject] text]];
                                                            [tableView reloadData];
                                                        }];
    
    [editController addAction: saveAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: @"Cancel"
                                                           style: UIAlertActionStyleCancel
                                                         handler: ^(UIAlertAction *action) {

                                                         }];
    [editController addAction: cancelAction];
    
    return editController;
}

-(UIAlertController *)addStudentToGroup:(Group *)group inCollectionView:(UICollectionView *)collectionView
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle: @"Add Student"
                                                                        message: @"What's the student's name?"
                                                                 preferredStyle: UIAlertControllerStyleAlert];
    
    [controller addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        textField.placeholder = @"Name";
        textField.textColor = [UIColor allportsBlue];
    }];
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle: @"Save"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction *action) {
                                                            [[StudentController SharedInstance] addStudent:[[controller.textFields firstObject] text] toGroup:group];
                                                            [collectionView reloadData];
                                                        }];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: @"Cancel"
                                                           style: UIAlertActionStyleCancel
                                                         handler: ^(UIAlertAction *action) {
                                                             [collectionView reloadData];
                                                         }];
    [controller addAction: cancelAction];
    [controller addAction: alertAction];
    return controller;
}

-(void)removeStudentAlertView:(Student *)student andCollectionView:(UICollectionView *)collectionView atTarget:(UIViewController *)target
{
    
    NSString *message = [NSString stringWithFormat:@"Are you sure that you want to delete %@ from this group?",student.name];
        NSString *title =[NSString stringWithFormat:@"Delete %@",student.name];
    
    UIAlertController *removeController = [UIAlertController alertControllerWithTitle: title
                                                                            message: message
                                                                     preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle: @"Delete"
                                                         style: UIAlertActionStyleDestructive
                                                       handler: ^(UIAlertAction *action) {
                                                           
                                                           [[StudentController SharedInstance] removeStudent:student];
                                                           [collectionView reloadData];
                                                       }];
    
    [removeController addAction: deleteAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle: @"Cancel"
                                                           style: UIAlertActionStyleCancel
                                                         handler: ^(UIAlertAction *action) {
                                                             
                                                         }];
    [removeController addAction: cancelAction];
    
    [target.navigationController presentViewController:removeController animated:YES completion:nil];
    
}


@end
